package com.vhausler.pt.controller.test;

import com.vhausler.pt.controller.impl.PrinterImpl;
import com.vhausler.pt.model.Payment;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Václav Häusler
 */
public class PrinterTest {

    private PrinterImpl printer;

    /**
     * Creates a new printer instance before the tests are run.
     */
    @Before
    public void setUp() {
        printer = new PrinterImpl();
    }

    /**
     * Tests for nullpointer exception when presented with null arg.
     */
    @Test(expected = NullPointerException.class)
    public void testAddNull() {
        printer.add(null);
    }

    /**
     * Tests for exceptions when using null/default values in the
     * {@link Payment} object.
     */
    @Test
    public void testAddNullValues() {
        int before = printer.getPayments().size();
        Payment p = new Payment();
        printer.add(p);
        int after = printer.getPayments().size();
        Assert.assertTrue(before < after);
    }

    /*
     Mores tests for the printer can be written here e.g. for the conversion methods.
     */
}
