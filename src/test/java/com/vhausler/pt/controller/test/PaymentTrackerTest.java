package com.vhausler.pt.controller.test;

import com.vhausler.pt.controller.impl.PaymentTrackerImpl;
import com.vhausler.pt.controller.impl.PrinterImpl;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Václav Häusler
 */
public class PaymentTrackerTest {

    private PaymentTrackerImpl pt;
    private PrinterImpl printer;

    /**
     * Creates new instance for PaymentTracker and Printer.
     */
    @Before
    public void setUp() {
        printer = new PrinterImpl();
        pt = new PaymentTrackerImpl(printer);
    }

    /**
     * Tests for nullpointer exception when create the PaymentTracker with null
     * arg.
     */
    @Test(expected = NullPointerException.class)
    public void constructorTest() {
        pt = new PaymentTrackerImpl(null);
    }

    /**
     * Tests the process line method, expects nullpointer when presented with
     * null arg.
     */
    @Test(expected = NullPointerException.class)
    public void testProcessLineNullInput() {
        pt.processLine(null);
    }

    /**
     * Tests the process line method for invalid input. The list of payments
     * should be the same size before and after the test. Tests the printer as
     * well (add, getPayments).
     */
    @Test
    public void testProcessLineInvalidInput() {
        int before = printer.getPayments().size();
        String line = "asdf";
        pt.processLine(line);
        int after = printer.getPayments().size();
        Assert.assertEquals(before, after);
    }

    /**
     * Tests the process line method for valid input. The list of payments
     * should have a different size before and after. Tests the printer as well
     * (add, getPayments).
     */
    @Test
    public void testProcessLineValidInput() {
        int before = printer.getPayments().size();
        String line = "USD 500";
        pt.processLine(line);
        int after = printer.getPayments().size();
        Assert.assertTrue(before < after);
    }

    /**
     * Tests the process line method for payment addition. Tests the printer as
     * well (add, getPayments).
     */
    @Test
    public void testProcessLineValidInputAddition() {
        String line = "USD 500";
        pt.processLine(line); // +500
        // avoid nullpointer and unnecessary if conditions
        int before = printer.getPayments().get("USD").getValue() - 500;
        pt.processLine(line); // +500
        int after = printer.getPayments().get("USD").getValue();
        Assert.assertTrue(after - before == 1000);
    }

    /**
     * Tests the process line method for payment deduction. Tests the printer as
     * well (add, getPayments).
     */
    @Test
    public void testProcessLineValidInputDeduction() {
        String lineAddition = "USD 500";
        String lineDeduction = "USD -500";
        pt.processLine(lineAddition); // +500
        pt.processLine(lineDeduction); // -500
        int after = printer.getPayments().get("USD").getValue();
        Assert.assertEquals(0, after);
    }

    /**
     * Tests the read file method, expects nullpointer when the method is
     * presented with null arg.
     */
    @Test(expected = NullPointerException.class)
    public void testReadFileNull() {
        pt.readFile(null);
    }

    /* 
     For further testing of readFile method the method itself should be more 
     atomic which was unnecessary for the purpose of the excercise.
    
     */
}
