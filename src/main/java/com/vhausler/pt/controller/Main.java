package com.vhausler.pt.controller;

import com.vhausler.pt.controller.impl.PrinterImpl;
import com.vhausler.pt.controller.impl.PaymentTrackerImpl;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/*
 - app that keeps record of payments
 - each payment includes a currency and an amount
 - output list of all the currency and amounts to the console once per minute
 - input = System.in, file
 - filename can be optionally specified
 - "quit" -> close
 - 0 amount = do not display
 - input <currency> <modifier><amount>
 -- currency = 3 letter code (USD, HKD, RMB, NZD, GBP)
 - for each assumption about errors, write it into a readme.txt

 - unit tests
 - threadsafe code
 - patterns

 - bitbucket
 - how to run info
 - display exchange rates to USD

 Sample input:
 USD 1000
 HKD 100
 USD -100
 RMB 2000
 HKD 200

 Sample output:
 USD 900
 RMB 2000
 HKD 300

Optional bonus question
Allow each currency to have the exchange rate compared to USD configured. When you display the output, write
the USD equivalent amount next to it, for example:
USD 900
RMB 2000 (USD 314.60)
HKD 300 (USD 38.62)

 notes: 
 - ignore capital letters, USD is the same like usd or USd
 - only int payments
 - try to recover from any errors
 */
/**
 * Main class that creates the runnables, submits them to the executor service
 * and canceles them when promted.
 *
 * @author Václav Häusler
 */
public class Main {

    /**
     * Number of threads availible to the thread pool.
     */
    private static final int THREADS = 2;
    /**
     * List of all runnables.
     */
    private static final List<Future> futures = new ArrayList<>();

    /**
     * Main method
     *
     * @param args no args expected
     */
    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(THREADS);

        PrinterImpl printer = new PrinterImpl();
        PaymentTrackerImpl pt = new PaymentTrackerImpl(printer);

        // expected to add runnables correctly
        futures.add(executor.submit(pt));
        futures.add(executor.submit(printer));
    }

    /**
     * Stops all the runnables.
     */
    public static void stopAllRunnables() {
        for (Future future : futures) {
            future.cancel(true);
        }
        System.exit(0);
    }
}
