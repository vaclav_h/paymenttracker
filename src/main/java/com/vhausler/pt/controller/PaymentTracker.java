package com.vhausler.pt.controller;

/**
 * Javadoc in the implementing class.
 *
 * @author Václav Häusler
 */
public interface PaymentTracker {

    public void processLine(String line) throws NullPointerException;

    public void readFile(String line) throws NullPointerException;

}
