package com.vhausler.pt.controller;

import com.vhausler.pt.model.Payment;
import java.util.Map;

/**
 * Javadoc in the implementing class.
 *
 * @author Václav Häusler
 */
public interface Printer {

    public void add(Payment payment);

    public void printPayments();

    public Map<String, Payment> getPayments();

    public Double getConversionToUSD(String currencyCode, int value);

    public Float getConversionRate(String currencyFrom, String currencyTo);

}
