package com.vhausler.pt.controller.impl;

import com.vhausler.pt.controller.Main;
import com.vhausler.pt.controller.PaymentTracker;
import com.vhausler.pt.model.Payment;
import com.vhausler.pt.utils.Constants;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * PaymentTracker is a class that takes care of all the input processing.
 * Creates payments, reads files, initiates the app shutdown when prompted.
 *
 * @author Václav Häusler
 */
public class PaymentTrackerImpl implements Runnable, PaymentTracker {

    /**
     * Flag that holds the information if the app should be running. False if
     * running.
     */
    static boolean quit = false;
    /**
     * Scanner to read the console input. Works boths for IDE and regular
     * console.
     */
    // platform specific, alternative API might be more useful here
    private final Scanner sc = new Scanner(System.in);
    /**
     * Printer to store the payments and print them.
     */
    private final PrinterImpl printer;

    /**
     * Constructor taking in the printer.
     *
     * @param printer {@link PrinterImpl}
     */
    public PaymentTrackerImpl(PrinterImpl printer) {
        if (printer == null) {
            throw new NullPointerException("printer cannot be null");
        }
        this.printer = printer;
    }

    /**
     * Processes all the input while the app is running.
     */
    @Override
    public void run() {
        String line;
        while (!quit) {
            // read the line
            line = sc.nextLine();
            if (line == null) {
                continue;
            }

            if (Constants.QUIT.equals(line)) {
                // initiate shutdown
                shutdown();
            } else if (line.startsWith(Constants.FILE)) {
                // read the file
                readFile(line);
            } else {
                // process the payment
                processLine(line);
            }
        }
    }

    /**
     * Validates the line and processes it. Throws nullpointer exception if null
     * input is presented. Method is public for easier testing.
     *
     * @param line string containing the payment information
     */
    public void processLine(String line) throws NullPointerException {
        if (line == null) {
            throw new NullPointerException("line cannot be null");
        }

        // match the pattern 'currency code consisting of 3 letters, 
        // whitespace, optional modifier minus, digits like 'USD -200'
        Pattern p = Pattern.compile(Constants.PAYMENT_LINE_PATTERN);
        Matcher m = p.matcher(line);
        if (m.find()) {

            // parse the line, nothing here should be null
            String currencyCode = line.substring(0, 3);
            boolean isDeduction = line.contains(Constants.MINUS_SIGN);
            int value;
            if (isDeduction) {
                value = Integer.parseInt(line.substring(line.indexOf(Constants.MINUS_SIGN)));
            } else {
                value = Integer.parseInt(line.substring(line.indexOf(Constants.WHITESPACE) + 1));
            }

            // create the payment
            Payment payment = new Payment();
            payment.setCurrencyCode(currencyCode.toUpperCase(Locale.US));
            payment.setValue(value);
            payment.setConversionToUSD(null);

            // add the payment so it can be printed out
            printer.add(payment);
        } else {
            // pattern not matched
            System.out.println("Invalid input. Example: USD -500");
        }
    }

    /**
     * Closes the application.
     */
    private void shutdown() {
        System.out.println("Closing the application");
        quit = true;
        Main.stopAllRunnables();
    }

    /**
     * Reads the file, when any errors are encountered throws no exceptions
     * (except for null input) and just prints info to the console to make the
     * operation easily recoverable from. Method is public for easier testing.
     *
     * @param line string containing the filename
     */
    public void readFile(String line) throws NullPointerException {
        if (line == null) {
            throw new NullPointerException("line cannot be null");
        }
        // match the pattern '-f filename' like '-f inputData'
        Pattern p = Pattern.compile(Constants.FILE_LINE_PATTERN);
        Matcher m = p.matcher(line);
        if (m.find()) {

            // split the line string to get the file name
            String filename = line.split(Constants.FILE + Constants.WHITESPACE)[1];

            // get the file
            File inputData = new File(filename);
            if (inputData.exists() && inputData.isFile()) {
                try {
                    System.out.println("File read successfuly");

                    // process all the loaded lines
                    List<String> lines = Files.readAllLines(inputData.toPath());
                    for (String l : lines) {
                        processLine(l);
                    }

                } catch (IOException ex) {
                    // file reading exception
                    System.out.println("Error reading file: " + filename + " Error code: " + ex.getMessage());
                }
            } else {
                // file not found
                System.out.println("Error reading file: " + filename);
            }
        } else {
            // pattern not matched
            System.out.println("Invalid input. Example: -f inputData");
        }
    }

}
