package com.vhausler.pt.controller.impl;

import com.vhausler.pt.controller.Printer;
import com.vhausler.pt.model.Payment;
import com.vhausler.pt.utils.Constants;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 * Printer class holds all the payments and prints the output to the console in
 * regular intervals specified in {@link Constants}
 *
 * @author Václav Häusler
 */
public class PrinterImpl implements Runnable, Printer {

    /**
     * Map of all the payments. The key is the currency code, the value is the
     * payment.
     */
    private final Map<String, Payment> payments;
    /**
     * The last time the printer printed out the payments to the console.
     */
    private long lastTick = System.currentTimeMillis();

    /**
     * Non-arg constructor.
     */
    public PrinterImpl() {
        payments = new HashMap<>();
    }

    /**
     * Prints out the list of all payments while the app is running.
     */
    @Override
    public void run() {
        while (!PaymentTrackerImpl.quit) {
            // print the payments if certain amount of time has passed
            if (System.currentTimeMillis() - lastTick > Constants.TICK_LIMIT) {
                printPayments();
                lastTick = System.currentTimeMillis();
            }
        }
    }

    /**
     * Adds a new payment. If the currency code is already contained in the map
     * it adds the payment value to the already existing one.
     *
     * @param payment {@link Payment}
     */
    public void add(Payment payment) throws NullPointerException {
        if (payment == null) {
            throw new NullPointerException("payment cannot be null");
        }
        if (payments.containsKey(payment.getCurrencyCode())) {
            // get the matching currency code
            Payment original = payments.get(payment.getCurrencyCode());

            // add/deduct the value
            int result = original.getValue() + payment.getValue();
            payment.setValue(result);
        }

        // stores the conversion to USD so it's faster to print out 
        // rather than to ask for each conversion when printing the payments
        if (!"USD".equals(payment.getCurrencyCode())) {
            Double conversion = getConversionToUSD(payment.getCurrencyCode(), payment.getValue());
            if (conversion != null) {
                payment.setConversionToUSD(conversion);
            }
        }

        // store or update the payment
        payments.put(payment.getCurrencyCode(), payment);
    }

    /**
     * Prints out all the payments except for those who have 0 value.
     */
    public void printPayments() {
        String straightLine = "_____________________________________________";
        if (payments.isEmpty()) {
            System.out.println("No payments yet");
        } else {
            System.out.println(straightLine);
            System.out.println("Payments: " + new Date());
            System.out.println(straightLine);
            for (Payment payment : payments.values()) {
                if (payment.getValue() != 0) {
                    System.out.println(payment);
                }
            }
            System.out.println(straightLine);
        }
    }

    /**
     * Returns all the payments. Used for testing.
     *
     * @return map of {@link String}, {@link Payment}
     */
    public Map<String, Payment> getPayments() {
        return payments;
    }

    /**
     * Convert the specific currency to USD
     *
     * @param currencyCode currency code
     * @param value value
     * @return converted value
     */
    public Double getConversionToUSD(String currencyCode, int value) {
        Double result;

        Float conversionRate = getConversionRate(currencyCode, "USD");
        if (conversionRate == null) {
            return null;
        }

        // calculate the conversion
        result = (double) (conversionRate * value);

        // round the result to 2 decimal digits
        result = round(result, 2);

        return result;
    }

    /**
     * Connect to Yahoo conversion service and fetch the conversion rate.
     *
     * @param currencyFrom currency from
     * @param currencyTo currency to
     * @return conversion rate
     */
    public Float getConversionRate(String currencyFrom, String currencyTo) {

        // exception for RMB which is Chinese Yuan but Yahoo doesn't 
        // recognize the currency code, when more exceptions like that come up,
        // create a separate method with a switch clause
        if ("RMB".equals(currencyFrom)) {
            currencyFrom = "CNY";
        }

        try {
            HttpClient client = HttpClientBuilder.create().build();
            String url = "http://quote.yahoo.com/d/quotes.csv?s=" + currencyFrom + currencyTo + "=X&f=l1&e=.csv";
            HttpGet request = new HttpGet(url);

            HttpResponse response = client.execute(request);

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuilder result = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            if (result.toString().contains("N/A")) {
                return null;
            }

            return Float.parseFloat(result.toString());
        } catch (IOException ex) {
            // conversion failed
        }
        return null;
    }

    /**
     * Round the double value to certain amount of decimal places.
     *
     * @param value double value
     * @param places decimal places
     * @return rounded value
     */
    public static double round(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
