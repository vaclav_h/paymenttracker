package com.vhausler.pt.model;

/**
 * Holds the information about a single payment.
 *
 * @author Václav Häusler
 */
public class Payment {

    /**
     * Currency code. e.g. USD
     */
    private String currencyCode;
    /**
     * Value which can be both negative and positive.
     */
    private int value;
    /**
     * Holds the conversion value to USD.
     */
    private Double conversionToUSD;

    /**
     * Retursn the currency code.
     *
     * @return currency code
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the currency code
     *
     * @param currencyCode currency code
     */
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    /**
     * Returns the value
     *
     * @return value
     */
    public int getValue() {
        return value;
    }

    /**
     * Sets the value.
     *
     * @param value value
     */
    public void setValue(int value) {
        this.value = value;
    }

    /**
     * Returns the conversion value.
     *
     * @return conversion value
     */
    public Double getConversionToUSD() {
        return conversionToUSD;
    }

    /**
     * Sets the conversion value.
     *
     * @param conversion conversion value
     */
    public void setConversionToUSD(Double conversion) {
        this.conversionToUSD = conversion;
    }

    /**
     * Returns a formatted string in the format of 'currency code, whitespace,
     * value'
     *
     * @return formatted payment string
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(currencyCode).append(" ").append(value);
        if (conversionToUSD != null) {
            sb.append("(USD ").append(conversionToUSD).append(")");
        }
        return sb.toString();
    }

}
