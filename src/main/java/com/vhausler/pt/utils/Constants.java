package com.vhausler.pt.utils;

/**
 * Holds all the constants.
 *
 * @author Václav Häusler
 */
public class Constants {

    /**
     * String to shutdown the app.
     */
    public static final String QUIT = "quit";
    /**
     * Minus sign used for string parsing.
     */
    public static final String MINUS_SIGN = "-";
    /**
     * Whitespace used for string parsing.
     */
    public static final String WHITESPACE = " ";
    /**
     * The amount of time before the printer prints all the payments.
     */
    public static final int TICK_LIMIT = 1000 * 60;
//    public static final int TICK_LIMIT = 10000;
    /**
     * String that determines a file should be loaded.
     */
    public static final String FILE = "-f";
    /**
     * Pattern for the payment string.
     */
    public static final String PAYMENT_LINE_PATTERN = "[a-zA-Z]{3}[\\s].\\d*$";
    /**
     * Pattern for the file string.
     */
    public static final String FILE_LINE_PATTERN = "[\\-f]*[\\s].*";

}
