#Building the project

##From the project folder

mvn clean compile assembly:single antrun:run


#Running the project

##After building the project

dist/run.bat

#Assumptions

- only integer inputs are allowed for currency
- lower/upper case does not matter if the currency input is formatted correctly

#Info

##If I was not working two jobs currently

- more tests could be written
- methods could be more abstract
- logger could be written
- better readme would be written as I had only few minutes here and there to work on this